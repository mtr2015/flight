<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2011, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */

/**
 * The Flight class is a static representation of the framework.
 *
 * Core.核心
 * @method  static void start() Starts the framework. 启动框架
 * @method  static void path($path) Adds a path for autoloading classes. 添加自动载入目录
 * @method  static void stop() Stops the framework and sends a response. 停止框架
 * @method  static void halt($code = 200, $message = '') Stop the framework with an optional status code and message. 中止框架并输出数字状态和相应信息
 *
 * Routing.路由
 * @method  static void route($pattern, $callback) Maps a URL pattern to a callback.映射URL模式和函数调用
 * @method  static \flight\net\Router router() Returns Router instance.返回一个Router实例
 *
 * Extending & Overriding.扩展和重写
 * @method  static void map($name, $callback) Creates a custom framework method.映射一个自定义的方法到框架
 * @method  static void register($name, $class, array $params = array(), $callback = null) Registers a class to a framework method. 注册一个类到框架
 *
 * Filtering. 过滤
 * @method  static void before($name, $callback) Adds a filter before a framework method.
 * @method  static void after($name, $callback) Adds a filter after a framework method.
 *
 * Variables. 变量
 * @method  static void set($key, $value) Sets a variable.
 * @method  static mixed get($key) Gets a variable.
 * @method  static bool has($key) Checks if a variable is set.
 * @method  static void clear($key = null) Clears a variable.
 *
 * Views. 视图
 * @method  static void render($file, array $data = null, $key = null) Renders a template file.
 * @method  static \flight\template\View view() Returns View instance.
 *
 * Request & Response. 请求和响应
 * @method  static \flight\net\Request request() Returns Request instance. 返回一个Request实例
 * @method  static \flight\net\Response response() Returns Response instance. 返回一个Response实例
 * @method  static void redirect($url, $code = 303) Redirects to another URL. 跳转到另外一个URL
 * @method  static void json($data, $code = 200, $encode = true, $charset = "utf8", $encodeOption = 0, $encodeDepth = 512) Sends a JSON response.
 * @method  static void jsonp($data, $param = 'jsonp', $code = 200, $encode = true, $charset = "utf8", $encodeOption = 0, $encodeDepth = 512) Sends a JSONP response.
 * @method  static void error($exception) Sends an HTTP 500 response. 500错误
 * @method  static void notFound() Sends an HTTP 404 response.  响应404请求
 *
 * HTTP Caching. HTTP缓存
 * @method  static void etag($id, $type = 'strong') Performs ETag HTTP caching.
 * @method  static void lastModified($time) Performs last modified HTTP caching.
 */
class Flight {
    /**
     * Framework engine.
     *
     * @var object
     */
    private static $engine;

    // Don't allow object instantiation
    private function __construct() {}
    private function __destruct() {}
    private function __clone() {}

    /**
     * Handles calls to static methods. 匹配调用到静态方法
     *
     * @param string $name Method name
     * @param array $params Method parameters
     * @return mixed Callback results
     * @throws \Exception
     */
    public static function __callStatic($name, $params) {
        $app = Flight::app();
        //在这里，Flight对Engine包装了一层而已。对Flight类静态函数的调用，实质上是对Engine类的相应函数的调用
        return \flight\core\Dispatcher::invokeMethod(array($app, $name), $params);
    }

    /**
     * @return \flight\Engine Application instance
     */
    public static function app() {
        static $initialized = false;

        if (!$initialized) {
//            $ti = $initialized;
//            var_dump($ti);die; // boolean false
            //这里定义框架的自动加载机制，实际上是依据PSR-0标准来做的  
            require_once __DIR__.'/autoload.php';
            //Engine类是框架的引擎所在
            self::$engine = new \flight\Engine();

            $initialized = true;
        }

        return self::$engine;
    }
}
