<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2013, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */

require_once __DIR__.'/core/Loader.php';

\flight\core\Loader::autoload(true, dirname(__DIR__));
//var_dump(__DIR__);die;
// 常量__DIR__，指向当前执行的PHP脚本所在的目录。
