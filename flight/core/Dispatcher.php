<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2011, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */
namespace flight\core;

/**
 * The Dispatcher class is responsible for dispatching events.
 * Events are simply aliases for class methods or functions. 
 * The Dispatcher allows you to hook other functions to an event that can modify the input parameters and/or the output.
 * Dispatcher类负责调度分发事件。事件是类方法或函数的别名。Dispatcher允许你关联函数到事件，以便用来修改传入或者输出的参数。
 */
class Dispatcher
{

    /**
     * Mapped events. 映射事件数组变量
     *
     * @var array
     */
    protected $events = array();

    /**
     * Method filters. 过滤方法数组变量
     *
     * @var array
     */
    protected $filters = array();

    /**
     * Dispatches an event. 分发一个事件
     *
     * @param string $name
     *            Event name
     * @param array $params
     *            Callback parameters
     * @return string Output of callback
     */
    public function run($name, array $params = array())
    {
        $output = '';

        // Run pre-filters 运行前置过滤函数
        if (! empty($this->filters[$name]['before'])) {
            $this->filter($this->filters[$name]['before'], $params, $output);
        }

        // Run requested method 运行请求的方法
        $output = $this->execute($this->get($name), $params);

        // Run post-filters  运行后置方法
        if (! empty($this->filters[$name]['after'])) {
            $this->filter($this->filters[$name]['after'], $params, $output);
        }

        return $output;
    }

    /**
     * Assigns a callback to an event.
     *
     * @param string $name
     *            Event name
     * @param callback $callback
     *            Callback function
     */
    public function set($name, $callback)
    {
        $this->events[$name] = $callback;
    }

    /**
     * Gets an assigned callback.
     *
     * @param string $name
     *            Event name
     * @return callback $callback Callback function
     */
    public function get($name)
    {
        return isset($this->events[$name]) ? $this->events[$name] : null;
    }

    /**
     * Checks if an event has been set.
     *
     * @param string $name
     *            Event name
     * @return bool Event status
     */
    public function has($name)
    {
        return isset($this->events[$name]);
    }

    /**
     * Clears an event.
     * If no name is given,
     * all events are removed.
     *
     * @param string $name
     *            Event name
     */
    public function clear($name = null)
    {
        if ($name !== null) {
            unset($this->events[$name]);
            unset($this->filters[$name]);
        } else {
            $this->events = array();
            $this->filters = array();
        }
    }

    /**
     * Hooks a callback to an event.
     *
     * @param string $name
     *            Event name
     * @param string $type
     *            Filter type
     * @param callback $callback
     *            Callback function
     */
    public function hook($name, $type, $callback)
    {
        $this->filters[$name][$type][] = $callback;
    }

    /**
     * Executes a chain of method filters.执行一系列的filters方法
     *
     * @param array $filters
     *            Chain of filters
     * @param array $params
     *            Method parameters
     * @param mixed $output
     *            Method output
     */
    public function filter($filters, &$params, &$output)
    {
        $args = array(
            &$params,
            &$output
        );
        foreach ($filters as $callback) {
            $continue = $this->execute($callback, $args);
            if ($continue === false)
                break;
        }
    }

    /**
     * Executes a callback function. 执行一个callback函数
     *
     * @param callback $callback
     *            Callback function
     * @param array $params
     *            Function parameters
     * @return mixed Function results
     * @throws \Exception
     */
    public static function execute($callback, array &$params = array())
    {
        if (is_callable($callback)) {
            return is_array($callback) ? self::invokeMethod($callback, $params) : self::callFunction($callback, $params);
        } else {
            throw new \Exception('Invalid callback specified.');
        }
    }

    /**
     * Calls a function. 调用一个函数
     *
     * @param string $func
     *            Name of function to call
     * @param array $params
     *            Function parameters
     * @return mixed Function results
     */
    public static function callFunction($func, array &$params = array())
    {
        // Call static method
        if (is_string($func) && strpos($func, '::') !== false) {
            return call_user_func_array($func, $params);
        }

        switch (count($params)) {
            case 0:
                return $func();
            case 1:
                return $func($params[0]);
            case 2:
                return $func($params[0], $params[1]);
            case 3:
                return $func($params[0], $params[1], $params[2]);
            case 4:
                return $func($params[0], $params[1], $params[2], $params[3]);
            case 5:
                return $func($params[0], $params[1], $params[2], $params[3], $params[4]);
            default:
                return call_user_func_array($func, $params);
        }
    }

    /**
     * Invokes a method. 调用一个方法
     *
     * @param mixed $func
     *            Class method 类方法
     * @param array $params
     *            Class method parameters 类方法参数
     * @return mixed Function results
     */
    public static function invokeMethod($func, array &$params = array())
    {
        list ($class, $method) = $func;

        $instance = is_object($class);

        switch (count($params)) {
            case 0:
                return ($instance) ? $class->$method() : $class::$method();
            case 1:
                return ($instance) ? $class->$method($params[0]) : $class::$method($params[0]);
            case 2:
                return ($instance) ? $class->$method($params[0], $params[1]) : $class::$method($params[0], $params[1]);
            case 3:
                return ($instance) ? $class->$method($params[0], $params[1], $params[2]) : $class::$method($params[0], $params[1], $params[2]);
            case 4:
                return ($instance) ? $class->$method($params[0], $params[1], $params[2], $params[3]) : $class::$method($params[0], $params[1], $params[2], $params[3]);
            case 5:  // 最多5个参数
                return ($instance) ? $class->$method($params[0], $params[1], $params[2], $params[3], $params[4]) : $class::$method($params[0], $params[1], $params[2], $params[3], $params[4]);
            default:
                return call_user_func_array($func, $params);
        }
    }

    /**
     * Resets the object to the initial state. 把object返回到初始状态
     */
    public function reset()
    {
        $this->events = array();
        $this->filters = array();
    }
}
