<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2011, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */
namespace flight\core;

/**
 * The Loader class is responsible for loading objects.
 * It maintains a list of reusable class instances and can generate a new class instances with custom initialization parameters. 
 * It also performs class autoloading.
 * Loader 类用来加载对象，负责维护可重复使用的类实例，同时也可生成新类实例（使用定制的初始化参数）。它同时扮演类自动加载。
 */
class Loader
{

    /**
     * Registered classes. 已经注册的类数组 $classes
     *
     * @var array
     */
    protected $classes = array();

    /**
     * Class instances.  类实例数组 $instances
     *
     * @var array
     */
    protected $instances = array();

    /**
     * Autoload directories. 自动加载目录数组 $dirs
     *
     * @var array
     */
    protected static $dirs = array();

    /**
     * Registers a class. 注册一个Class
     *
     * @param string $name
     *            Registry name 注册名称
     * @param string|callable $class
     *            Class name or function to instantiate class 需要实例化的类名或者函数
     * @param array $params
     *            Class initialization parameters 类实例化参数
     * @param callback $callback
     *            Function to call after object instantiation 对象实例化过后调用的函数
     */
    public function register($name, $class, array $params = array(), $callback = null)
    {
        unset($this->instances[$name]); // 要注册，先清空，确保永远只有最新的一份数据
        
        $this->classes[$name] = array(
            $class,
            $params,
            $callback
        );
    }

    /**
     * Unregisters a class. 取消注册一个类
     *
     * @param string $name
     *            Registry name  注册的名称
     */
    public function unregister($name)
    {
        unset($this->classes[$name]);
    }

    /**
     * Loads a registered class. 载入注册好的class
     *
     * @param string $name
     *            Method name 方法名
     * @param bool $shared
     *            Shared instance 共享实例
     * @return object Class instance  对象类实例
     */
    public function load($name, $shared = true)
    {
        $obj = null;
        
        if (isset($this->classes[$name])) {
            list ($class, $params, $callback) = $this->classes[$name];
            
            $exists = isset($this->instances[$name]);
            
            if ($shared) { // 共享实例
                $obj = ($exists) ? $this->getInstance($name) : $this->newInstance($class, $params);
                
                if (! $exists) {  // 加入共享实例
                    $this->instances[$name] = $obj;
                }
            } else { // 不是共享实例
                $obj = $this->newInstance($class, $params);
            }
            
            if ($callback && (! $shared || ! $exists)) {
                $ref = array(
                    &$obj
                );
                call_user_func_array($callback, $ref);
            }
        }
        
        return $obj;
    }

    /**
     * Gets a single instance of a class. 取得一个类的单例
     *
     * @param string $name
     *            Instance name
     * @return object Class instance
     */
    public function getInstance($name)
    {
        return isset($this->instances[$name]) ? $this->instances[$name] : null;
    }

    /**
     * Gets a new instance of a class. 获取一个类的新实例
     *
     * @param string|callable $class
     *            Class name or callback function to instantiate class
     * @param array $params
     *            Class initialization parameters
     * @return object Class instance
     */
    public function newInstance($class, array $params = array())
    {
        if (is_callable($class)) {
            return call_user_func_array($class, $params);
        }
        
        switch (count($params)) {
            case 0:
                return new $class();
            case 1:
                return new $class($params[0]);
            case 2:
                return new $class($params[0], $params[1]);
            case 3:
                return new $class($params[0], $params[1], $params[2]);
            case 4:
                return new $class($params[0], $params[1], $params[2], $params[3]);
            case 5:
                return new $class($params[0], $params[1], $params[2], $params[3], $params[4]);  // 最多传入5个参数
            default:
                $refClass = new \ReflectionClass($class);  // 反射用来做类的实例化
                return $refClass->newInstanceArgs($params);  // 用传入的参数，获得一个类的新实例
        }
    }

    /**
     *
     * @param string $name
     *            Registry name
     * @return mixed Class information or null if not registered
     */
    public function get($name)
    {
        return isset($this->classes[$name]) ? $this->classes[$name] : null;
    }

    /**
     * Resets the object to the initial state.
     */
    public function reset()
    {
        $this->classes = array();
        $this->instances = array();
    }

    /**
     * * Autoloading Functions **
     * 自动加载函数
     */
    
    /**
     * Starts/stops autoloader.
     *
     * @param bool $enabled 
     *            Enable/disable autoloading
     * @param mixed $dirs
     *            Autoload directories
     */
    public static function autoload($enabled = true, $dirs = array())
    {
        if ($enabled) {
            spl_autoload_register(array(
                __CLASS__,          //  __CLASS__获取当前的类名
                'loadClass'
            ));
        } else {
            spl_autoload_unregister(array(
                __CLASS__,
                'loadClass'
            ));
        }
        
        if (! empty($dirs)) {
            self::addDirectory($dirs);
        }
    }

    /**
     * Autoloads classes.  自动加载类
     *
     * @param string $class
     *            Class name
     */
    public static function loadClass($class)
    {
        $class_file = str_replace(array(
            '\\',
            '_'
        ), '/', $class) . '.php';
        
        foreach (self::$dirs as $dir) {
            $file = $dir . '/' . $class_file;
            if (file_exists($file)) {
                require $file;
                return;  // 为什么这个地方要加个 return ？？？？？？
            }
        }
    }

    /**
     * Adds a directory for autoloading classes. 添加自动加载目录，用来加载类
     *
     * @param mixed $dir
     *            Directory path
     */
    public static function addDirectory($dir)
    {
        if (is_array($dir) || is_object($dir)) {
            foreach ($dir as $value) {
                self::addDirectory($value);
            }
        } else 
            if (is_string($dir)) {
                if (! in_array($dir, self::$dirs))
                    self::$dirs[] = $dir;  // 新增目录
            }
    }
}
