<?php
require 'flight/Flight.php';

 Flight::route('/', function(){
     echo 'hello flight!';
 });

//Flight::route('/@name/@id', function($name,$id){
//    echo "hello, $name ($id)!";
//});

//class Greeting {
//    public static function hello() {
//        echo 'hello world11111111!';
//    }
//}
//
// Flight::route('/',['Greeting','hello']);

// Flight::route('/user/[0-9]+', function(){
// // This will match /user/1234
// echo "123";
// });

// function hello(){
// echo 'hello world 123!';
// }
//
// Flight::route('/', 'hello');

// class Greeting
// {
// public function __construct() {
// $this->name = 'John Doe';
// }
//
// public function hello() {
// echo "Hello, {$this->name}!";
// }
// }
//
// $greeting = new Greeting();
//
// Flight::route('/', array($greeting, 'hello'));

// Flight::route('/blog/*', function(){
// // This will match /blog/2000/02/01
// echo "123456 blog";
// });

// Flight::route('/blog(/@year(/@month(/@day)))', function($year, $month, $day){
// This will match the following URLS:
// /blog/2012/12/10
// /blog/2012/12
// /blog/2012
// /blog
// echo "123";
// });

// Flight::route('/@name/@id', function($name, $id){
// echo "hello, $name ($id)!";
// });

// Flight::route('*', function($name, $id){
// echo "hello, $name ($id)!";
// });
// --------------------------------------------------------------
// Flight::route('/user/@name', function($name){
// // 检查某些条件
// if ($name != "Bob") {
// // 延续到下一个路由
// return true;
// }
// });
//
// Flight::route('/user/*', function(){
// // 这里会被调用到
// echo '123';
// });
// ------------------------------------------------------------------
// Flight::route('/', function($route){
// // 匹配到的HTTP方法的数组
// $route->methods;

// // 命名参数数组
// $route->params;

// // 匹配的正则表达式
// $route->regex;

// // Contains the contents of any '*' used in the URL pattern
// $route->splat;
// // var_dump($route);
// }, true);

//---------------------------------------------------
// Flight::route('GET /', function () {
//     echo 'Our Home Page';
// });
// Flight::route('GET /users', function () {
//     echo 'A list of users';
// });
// Flight::route('POST /users', function () {
//     echo 'Adding a user';
// });
// Flight::route('/users/[0-9]+', function () {
//     echo 'This is a specific user';
// });
//-------------------------
// Flight::route('GET /users/[0-9]', function( $route ){
//     var_dump( $route->regex );
// }, true);
//---------------------------------




Flight::start();
